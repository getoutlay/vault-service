# vault-service project

This project uses Reactive Spring Framework.

If you want to learn more about Reactive Spring, please visit: https://spring.io/reactive .

## Model

```plantuml

class Account {
  + String name
  + String institution
  + String accountNumber
  + Boolean expenseAccount
  + Currency currency
  + Transaction transactions
  + String readerName
}


class Category {
  + String name
  + String colorCode
}


class Transaction {
  + Account account
  + String desription
  + Double amount
  + Currency currency
  + Datetime bookedOn
  + Datetime executedOn
  + String explanation
  + String reference
  + String corresponderName
  + String corresponderBank
  + String corresponderAccount
}
Transaction "*" *--o "1" Account : account

class TransactionCategory {
  + Transaction transaction
  + Category category
  + Datetime assignedOn
  + String assignedBy
}
TransactionCategory "*" *--o "1" Transaction : transaction
TransactionCategory "*" *--o "1" Category : category

```

[//]: # "mastic-pin-readme"
