package io.gitlab.getoutlay;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@ControllerTest
public class TransactionCategoryControllerTest {

  @Autowired
  ApplicationContext context;

  @Autowired
  TestData data;

  @Autowired
  TransactionCategoryRepository repository;

  WebTestClient rest;

  @BeforeEach
  public void setUp() {
    rest = WebTestClient.bindToApplicationContext(context)
        .configureClient()
        .responseTimeout(Duration.ofHours(1L))
        .build();
  }

  @Test
  public void createGetAndList() {

    TransactionCategory entity = new TransactionCategory();
    entity.setTransactionId(data.getTransaction().getId());
    entity.setCategoryId(data.getCategory().getId());
    entity.setAssignedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setAssignedBy("Malayan Tapir");

    TransactionCategory created = rest.post()
        .uri("/transaction-categories")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(TransactionCategory.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getTransaction().getId(), created.getTransactionId());
    assertEquals(data.getCategory().getId(), created.getCategoryId());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getAssignedOn());
    assertEquals("Malayan Tapir", created.getAssignedBy());

    TransactionCategory single = rest.get()
        .uri("/transaction-categories/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(TransactionCategory.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());

    List<TransactionCategory> list = rest.get()
        .uri("/transaction-categories")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(new ParameterizedTypeReference<List<TransactionCategory>>() {
        })
        .returnResult()
        .getResponseBody();

    assertNotNull(list);
    assertThat(list, not(empty()));
    assertThat(created, is(in(list)));
  }

  @Test
  public void createUpdateAndGet() {

    TransactionCategory entity = new TransactionCategory();
    entity.setTransactionId(data.getTransaction().getId());
    entity.setCategoryId(data.getCategory().getId());
    entity.setAssignedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setAssignedBy("Malayan Tapir");

    TransactionCategory created = rest.post()
        .uri("/transaction-categories")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(TransactionCategory.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getTransaction().getId(), created.getTransactionId());
    assertEquals(data.getCategory().getId(), created.getCategoryId());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getAssignedOn());
    assertEquals("Malayan Tapir", created.getAssignedBy());

    entity.setTransactionId(data.getTransaction().getId());
    entity.setCategoryId(data.getCategory().getId());
    entity.setAssignedOn(Instant.parse("2020-02-02T10:00:00.00Z"));
    entity.setAssignedBy("Malayan Tapir Updated");

    TransactionCategory updated = rest.put()
        .uri("/transaction-categories/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(TransactionCategory.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(updated);
    assertEquals(data.getTransaction().getId(), updated.getTransactionId());
    assertEquals(data.getCategory().getId(), updated.getCategoryId());
    assertEquals(Instant.parse("2020-02-02T10:00:00.00Z"), updated.getAssignedOn());
    assertEquals("Malayan Tapir Updated", updated.getAssignedBy());

    TransactionCategory single = rest.get()
        .uri("/transaction-categories/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(TransactionCategory.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());
  }

  @Test
  public void createDeleteAndGet() {

    TransactionCategory entity = new TransactionCategory();
    entity.setTransactionId(data.getTransaction().getId());
    entity.setCategoryId(data.getCategory().getId());
    entity.setAssignedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setAssignedBy("Malayan Tapir");

    TransactionCategory created = rest.post()
        .uri("/transaction-categories")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(TransactionCategory.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getTransaction().getId(), created.getTransactionId());
    assertEquals(data.getCategory().getId(), created.getCategoryId());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getAssignedOn());
    assertEquals("Malayan Tapir", created.getAssignedBy());

    TransactionCategory deleted = rest.delete()
        .uri("/transaction-categories/{id}", created.getId())
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(TransactionCategory.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(deleted);
    assertEquals(created.getId(), deleted.getId());

    rest.get()
        .uri("/transaction-categories/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getInvalid() {
    rest.get()
        .uri("/transaction-categories/{id}", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissing() {
    rest.get()
        .uri("/transaction-categories/{id}", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  @Test
  public void getTransaction() {
    TransactionCategory entity = new TransactionCategory();
    entity.setTransactionId(data.getTransaction().getId());
    entity.setCategoryId(data.getCategory().getId());
    entity.setAssignedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setAssignedBy("Malayan Tapir");

    TransactionCategory transactionCategory = Objects.requireNonNull(repository.save(entity).block());

    Transaction result = rest.get()
        .uri("/transaction-categories/{id}/transaction", transactionCategory.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(result);
    assertEquals(result.getId(), result.getId());
  }

  @Test
  public void getInvalidTransaction() {
    rest.get()
        .uri("/transaction-categories/{id}/transaction", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissingTransaction() {
    rest.get()
        .uri("/transaction-categories/{id}/transaction", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  @Test
  public void getCategory() {
    TransactionCategory entity = new TransactionCategory();
    entity.setTransactionId(data.getTransaction().getId());
    entity.setCategoryId(data.getCategory().getId());
    entity.setAssignedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setAssignedBy("Malayan Tapir");

    TransactionCategory transactionCategory = Objects.requireNonNull(repository.save(entity).block());

    Category result = rest.get()
        .uri("/transaction-categories/{id}/category", transactionCategory.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Category.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(result);
    assertEquals(result.getId(), result.getId());
  }

  @Test
  public void getInvalidCategory() {
    rest.get()
        .uri("/transaction-categories/{id}/category", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissingCategory() {
    rest.get()
        .uri("/transaction-categories/{id}/category", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  
}
