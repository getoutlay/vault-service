package io.gitlab.getoutlay;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@ControllerTest
public class TransactionControllerTest {

  @Autowired
  ApplicationContext context;

  @Autowired
  TestData data;

  @Autowired
  TransactionRepository repository;

  WebTestClient rest;

  @BeforeEach
  public void setUp() {
    rest = WebTestClient.bindToApplicationContext(context)
        .configureClient()
        .responseTimeout(Duration.ofHours(1L))
        .build();
  }

  @Test
  public void createGetAndList() {

    Transaction entity = new Transaction();
    entity.setAccountId(data.getAccount().getId());
    entity.setDesription("Malayan Tapir");
    entity.setAmount(87934588315.2384);
    entity.setCurrency(Currency.values()[0]);
    entity.setBookedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setExecutedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setExplanation("Malayan Tapir");
    entity.setReference("Malayan Tapir");
    entity.setCorresponderName("Malayan Tapir");
    entity.setCorresponderBank("Malayan Tapir");
    entity.setCorresponderAccount("Malayan Tapir");

    Transaction created = rest.post()
        .uri("/transactions")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getAccount().getId(), created.getAccountId());
    assertEquals("Malayan Tapir", created.getDesription());
    assertEquals(87934588315.2384, created.getAmount());
    assertEquals(Currency.values()[0], created.getCurrency());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getBookedOn());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getExecutedOn());
    assertEquals("Malayan Tapir", created.getExplanation());
    assertEquals("Malayan Tapir", created.getReference());
    assertEquals("Malayan Tapir", created.getCorresponderName());
    assertEquals("Malayan Tapir", created.getCorresponderBank());
    assertEquals("Malayan Tapir", created.getCorresponderAccount());

    Transaction single = rest.get()
        .uri("/transactions/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());

    List<Transaction> list = rest.get()
        .uri("/transactions")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(new ParameterizedTypeReference<List<Transaction>>() {
        })
        .returnResult()
        .getResponseBody();

    assertNotNull(list);
    assertThat(list, not(empty()));
    assertThat(created, is(in(list)));
  }

  @Test
  public void createUpdateAndGet() {

    Transaction entity = new Transaction();
    entity.setAccountId(data.getAccount().getId());
    entity.setDesription("Malayan Tapir");
    entity.setAmount(87934588315.2384);
    entity.setCurrency(Currency.values()[0]);
    entity.setBookedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setExecutedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setExplanation("Malayan Tapir");
    entity.setReference("Malayan Tapir");
    entity.setCorresponderName("Malayan Tapir");
    entity.setCorresponderBank("Malayan Tapir");
    entity.setCorresponderAccount("Malayan Tapir");

    Transaction created = rest.post()
        .uri("/transactions")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getAccount().getId(), created.getAccountId());
    assertEquals("Malayan Tapir", created.getDesription());
    assertEquals(87934588315.2384, created.getAmount());
    assertEquals(Currency.values()[0], created.getCurrency());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getBookedOn());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getExecutedOn());
    assertEquals("Malayan Tapir", created.getExplanation());
    assertEquals("Malayan Tapir", created.getReference());
    assertEquals("Malayan Tapir", created.getCorresponderName());
    assertEquals("Malayan Tapir", created.getCorresponderBank());
    assertEquals("Malayan Tapir", created.getCorresponderAccount());

    entity.setAccountId(data.getAccount().getId());
    entity.setDesription("Malayan Tapir Updated");
    entity.setAmount(87934588357.2384);
    entity.setCurrency(Currency.values()[0]);
    entity.setBookedOn(Instant.parse("2020-02-02T10:00:00.00Z"));
    entity.setExecutedOn(Instant.parse("2020-02-02T10:00:00.00Z"));
    entity.setExplanation("Malayan Tapir Updated");
    entity.setReference("Malayan Tapir Updated");
    entity.setCorresponderName("Malayan Tapir Updated");
    entity.setCorresponderBank("Malayan Tapir Updated");
    entity.setCorresponderAccount("Malayan Tapir Updated");

    Transaction updated = rest.put()
        .uri("/transactions/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(updated);
    assertEquals(data.getAccount().getId(), updated.getAccountId());
    assertEquals("Malayan Tapir Updated", updated.getDesription());
    assertEquals(87934588357.2384, updated.getAmount());
    assertEquals(Currency.values()[0], updated.getCurrency());
    assertEquals(Instant.parse("2020-02-02T10:00:00.00Z"), updated.getBookedOn());
    assertEquals(Instant.parse("2020-02-02T10:00:00.00Z"), updated.getExecutedOn());
    assertEquals("Malayan Tapir Updated", updated.getExplanation());
    assertEquals("Malayan Tapir Updated", updated.getReference());
    assertEquals("Malayan Tapir Updated", updated.getCorresponderName());
    assertEquals("Malayan Tapir Updated", updated.getCorresponderBank());
    assertEquals("Malayan Tapir Updated", updated.getCorresponderAccount());

    Transaction single = rest.get()
        .uri("/transactions/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());
  }

  @Test
  public void createDeleteAndGet() {

    Transaction entity = new Transaction();
    entity.setAccountId(data.getAccount().getId());
    entity.setDesription("Malayan Tapir");
    entity.setAmount(87934588315.2384);
    entity.setCurrency(Currency.values()[0]);
    entity.setBookedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setExecutedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setExplanation("Malayan Tapir");
    entity.setReference("Malayan Tapir");
    entity.setCorresponderName("Malayan Tapir");
    entity.setCorresponderBank("Malayan Tapir");
    entity.setCorresponderAccount("Malayan Tapir");

    Transaction created = rest.post()
        .uri("/transactions")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getAccount().getId(), created.getAccountId());
    assertEquals("Malayan Tapir", created.getDesription());
    assertEquals(87934588315.2384, created.getAmount());
    assertEquals(Currency.values()[0], created.getCurrency());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getBookedOn());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getExecutedOn());
    assertEquals("Malayan Tapir", created.getExplanation());
    assertEquals("Malayan Tapir", created.getReference());
    assertEquals("Malayan Tapir", created.getCorresponderName());
    assertEquals("Malayan Tapir", created.getCorresponderBank());
    assertEquals("Malayan Tapir", created.getCorresponderAccount());

    Transaction deleted = rest.delete()
        .uri("/transactions/{id}", created.getId())
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(deleted);
    assertEquals(created.getId(), deleted.getId());

    rest.get()
        .uri("/transactions/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getInvalid() {
    rest.get()
        .uri("/transactions/{id}", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissing() {
    rest.get()
        .uri("/transactions/{id}", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  @Test
  public void getAccount() {
    Transaction entity = new Transaction();
    entity.setAccountId(data.getAccount().getId());
    entity.setDesription("Malayan Tapir");
    entity.setAmount(87934588315.2384);
    entity.setCurrency(Currency.values()[0]);
    entity.setBookedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setExecutedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setExplanation("Malayan Tapir");
    entity.setReference("Malayan Tapir");
    entity.setCorresponderName("Malayan Tapir");
    entity.setCorresponderBank("Malayan Tapir");
    entity.setCorresponderAccount("Malayan Tapir");

    Transaction transaction = Objects.requireNonNull(repository.save(entity).block());

    Account result = rest.get()
        .uri("/transactions/{id}/account", transaction.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Account.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(result);
    assertEquals(result.getId(), result.getId());
  }

  @Test
  public void getInvalidAccount() {
    rest.get()
        .uri("/transactions/{id}/account", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissingAccount() {
    rest.get()
        .uri("/transactions/{id}/account", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  
}
