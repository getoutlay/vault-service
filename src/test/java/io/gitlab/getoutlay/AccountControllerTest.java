package io.gitlab.getoutlay;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@ControllerTest
public class AccountControllerTest {

  @Autowired
  ApplicationContext context;

  @Autowired
  TestData data;

  @Autowired
  AccountRepository repository;

  WebTestClient rest;

  @BeforeEach
  public void setUp() {
    rest = WebTestClient.bindToApplicationContext(context)
        .configureClient()
        .responseTimeout(Duration.ofHours(1L))
        .build();
  }

  @Test
  public void createGetAndList() {

    Account entity = new Account();
    entity.setName("Malayan Tapir");
    entity.setInstitution("Malayan Tapir");
    entity.setAccountNumber("Malayan Tapir");
    entity.setExpenseAccount(false);
    entity.setCurrency(Currency.values()[0]);
    entity.setReaderName("Malayan Tapir");

    Account created = rest.post()
        .uri("/accounts")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Account.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getName());
    assertEquals("Malayan Tapir", created.getInstitution());
    assertEquals("Malayan Tapir", created.getAccountNumber());
    assertEquals(false, created.getExpenseAccount());
    assertEquals(Currency.values()[0], created.getCurrency());
    assertEquals("Malayan Tapir", created.getReaderName());

    Account single = rest.get()
        .uri("/accounts/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Account.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());

    List<Account> list = rest.get()
        .uri("/accounts")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(new ParameterizedTypeReference<List<Account>>() {
        })
        .returnResult()
        .getResponseBody();

    assertNotNull(list);
    assertThat(list, not(empty()));
    assertThat(created, is(in(list)));
  }

  @Test
  public void createUpdateAndGet() {

    Account entity = new Account();
    entity.setName("Malayan Tapir");
    entity.setInstitution("Malayan Tapir");
    entity.setAccountNumber("Malayan Tapir");
    entity.setExpenseAccount(false);
    entity.setCurrency(Currency.values()[0]);
    entity.setReaderName("Malayan Tapir");

    Account created = rest.post()
        .uri("/accounts")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Account.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getName());
    assertEquals("Malayan Tapir", created.getInstitution());
    assertEquals("Malayan Tapir", created.getAccountNumber());
    assertEquals(false, created.getExpenseAccount());
    assertEquals(Currency.values()[0], created.getCurrency());
    assertEquals("Malayan Tapir", created.getReaderName());

    entity.setName("Malayan Tapir Updated");
    entity.setInstitution("Malayan Tapir Updated");
    entity.setAccountNumber("Malayan Tapir Updated");
    entity.setExpenseAccount(true);
    entity.setCurrency(Currency.values()[0]);
    entity.setReaderName("Malayan Tapir Updated");

    Account updated = rest.put()
        .uri("/accounts/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Account.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(updated);
    assertEquals("Malayan Tapir Updated", updated.getName());
    assertEquals("Malayan Tapir Updated", updated.getInstitution());
    assertEquals("Malayan Tapir Updated", updated.getAccountNumber());
    assertEquals(true, updated.getExpenseAccount());
    assertEquals(Currency.values()[0], updated.getCurrency());
    assertEquals("Malayan Tapir Updated", updated.getReaderName());

    Account single = rest.get()
        .uri("/accounts/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Account.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());
  }

  @Test
  public void createDeleteAndGet() {

    Account entity = new Account();
    entity.setName("Malayan Tapir");
    entity.setInstitution("Malayan Tapir");
    entity.setAccountNumber("Malayan Tapir");
    entity.setExpenseAccount(false);
    entity.setCurrency(Currency.values()[0]);
    entity.setReaderName("Malayan Tapir");

    Account created = rest.post()
        .uri("/accounts")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Account.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getName());
    assertEquals("Malayan Tapir", created.getInstitution());
    assertEquals("Malayan Tapir", created.getAccountNumber());
    assertEquals(false, created.getExpenseAccount());
    assertEquals(Currency.values()[0], created.getCurrency());
    assertEquals("Malayan Tapir", created.getReaderName());

    Account deleted = rest.delete()
        .uri("/accounts/{id}", created.getId())
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Account.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(deleted);
    assertEquals(created.getId(), deleted.getId());

    rest.get()
        .uri("/accounts/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getInvalid() {
    rest.get()
        .uri("/accounts/{id}", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissing() {
    rest.get()
        .uri("/accounts/{id}", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  
  @Test
  public void createGetAndListTransactions() {
    Transaction transactions = new Transaction();
    transactions.setAccountId(data.getAccount().getId());
    transactions.setDesription("Malayan Tapir");
    transactions.setAmount(87934588315.2384);
    transactions.setCurrency(Currency.values()[0]);
    transactions.setBookedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    transactions.setExecutedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    transactions.setExplanation("Malayan Tapir");
    transactions.setReference("Malayan Tapir");
    transactions.setCorresponderName("Malayan Tapir");
    transactions.setCorresponderBank("Malayan Tapir");
    transactions.setCorresponderAccount("Malayan Tapir");

    Transaction created = rest.post()
        .uri("/accounts/{accountId}/transactions", data.getAccount().getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(transactions)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);

    Transaction single = rest.get()
        .uri("/accounts/{accountId}/transactions/{transactionsId}", data.getAccount().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());

    List<Transaction> list = rest.get()
        .uri("/accounts/{accountId}/transactions", data.getAccount().getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(new ParameterizedTypeReference<List<Transaction>>() {
        })
        .returnResult()
        .getResponseBody();

    assertNotNull(list);
    assertThat(list, not(empty()));
    assertThat(created, is(in(list)));
  }

  @Test
  public void createUpdateAndGetTransactions() {
    Transaction transactions = new Transaction();
    transactions.setAccountId(data.getAccount().getId());
    transactions.setDesription("Malayan Tapir");
    transactions.setAmount(87934588315.2384);
    transactions.setCurrency(Currency.values()[0]);
    transactions.setBookedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    transactions.setExecutedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    transactions.setExplanation("Malayan Tapir");
    transactions.setReference("Malayan Tapir");
    transactions.setCorresponderName("Malayan Tapir");
    transactions.setCorresponderBank("Malayan Tapir");
    transactions.setCorresponderAccount("Malayan Tapir");

    Transaction created = rest.post()
        .uri("/accounts/{accountId}/transactions", data.getAccount().getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(transactions)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getAccount().getId(), created.getAccountId());
    assertEquals("Malayan Tapir", created.getDesription());
    assertEquals(87934588315.2384, created.getAmount());
    assertEquals(Currency.values()[0], created.getCurrency());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getBookedOn());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getExecutedOn());
    assertEquals("Malayan Tapir", created.getExplanation());
    assertEquals("Malayan Tapir", created.getReference());
    assertEquals("Malayan Tapir", created.getCorresponderName());
    assertEquals("Malayan Tapir", created.getCorresponderBank());
    assertEquals("Malayan Tapir", created.getCorresponderAccount());

    transactions.setDesription("Malayan Tapir Updated");
    transactions.setAmount(87934588357.2384);
    transactions.setCurrency(Currency.values()[0]);
    transactions.setBookedOn(Instant.parse("2020-02-02T10:00:00.00Z"));
    transactions.setExecutedOn(Instant.parse("2020-02-02T10:00:00.00Z"));
    transactions.setExplanation("Malayan Tapir Updated");
    transactions.setReference("Malayan Tapir Updated");
    transactions.setCorresponderName("Malayan Tapir Updated");
    transactions.setCorresponderBank("Malayan Tapir Updated");
    transactions.setCorresponderAccount("Malayan Tapir Updated");

    Transaction updated = rest.put()
        .uri("/accounts/{accountId}/transactions/{transactionsId}", data.getAccount().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(transactions)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(updated);
    assertEquals(data.getAccount().getId(), updated.getAccountId());
    assertEquals("Malayan Tapir Updated", updated.getDesription());
    assertEquals(87934588357.2384, updated.getAmount());
    assertEquals(Currency.values()[0], updated.getCurrency());
    assertEquals(Instant.parse("2020-02-02T10:00:00.00Z"), updated.getBookedOn());
    assertEquals(Instant.parse("2020-02-02T10:00:00.00Z"), updated.getExecutedOn());
    assertEquals("Malayan Tapir Updated", updated.getExplanation());
    assertEquals("Malayan Tapir Updated", updated.getReference());
    assertEquals("Malayan Tapir Updated", updated.getCorresponderName());
    assertEquals("Malayan Tapir Updated", updated.getCorresponderBank());
    assertEquals("Malayan Tapir Updated", updated.getCorresponderAccount());

    Transaction single = rest.get()
        .uri("/accounts/{accountId}/transactions/{transactionsId}", data.getAccount().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());
  }

  @Test
  public void createDeleteAndGetTransactions() {
    Transaction transactions = new Transaction();
    transactions.setAccountId(data.getAccount().getId());
    transactions.setDesription("Malayan Tapir");
    transactions.setAmount(87934588315.2384);
    transactions.setCurrency(Currency.values()[0]);
    transactions.setBookedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    transactions.setExecutedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
    transactions.setExplanation("Malayan Tapir");
    transactions.setReference("Malayan Tapir");
    transactions.setCorresponderName("Malayan Tapir");
    transactions.setCorresponderBank("Malayan Tapir");
    transactions.setCorresponderAccount("Malayan Tapir");

    Transaction created = rest.post()
        .uri("/accounts/{accountId}/transactions", data.getAccount().getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(transactions)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getAccount().getId(), created.getAccountId());
    assertEquals("Malayan Tapir", created.getDesription());
    assertEquals(87934588315.2384, created.getAmount());
    assertEquals(Currency.values()[0], created.getCurrency());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getBookedOn());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getExecutedOn());
    assertEquals("Malayan Tapir", created.getExplanation());
    assertEquals("Malayan Tapir", created.getReference());
    assertEquals("Malayan Tapir", created.getCorresponderName());
    assertEquals("Malayan Tapir", created.getCorresponderBank());
    assertEquals("Malayan Tapir", created.getCorresponderAccount());

    Transaction deleted = rest.delete()
        .uri("/accounts/{accountId}/transactions/{transactionsId}", data.getAccount().getId(), created.getId())
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Transaction.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(deleted);
    assertEquals(created.getId(), deleted.getId());

    rest.get()
        .uri("/accounts/{accountId}/transactions/{transactionsId}", data.getAccount().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getInvalidTransactions() {
    Account entity = new Account();
    entity.setName("Malayan Tapir");
    entity.setInstitution("Malayan Tapir");
    entity.setAccountNumber("Malayan Tapir");
    entity.setExpenseAccount(false);
    entity.setCurrency(Currency.values()[0]);
    entity.setReaderName("Malayan Tapir");
    Account account = Objects.requireNonNull(repository.save(entity).block());

    rest.get()
        .uri("/accounts/{accountId}/transactions/{transactionsId}", data.getAccount().getId(), "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissingTransactions() {
    Account entity = new Account();
    entity.setName("Malayan Tapir");
    entity.setInstitution("Malayan Tapir");
    entity.setAccountNumber("Malayan Tapir");
    entity.setExpenseAccount(false);
    entity.setCurrency(Currency.values()[0]);
    entity.setReaderName("Malayan Tapir");
    Account account = Objects.requireNonNull(repository.save(entity).block());

    rest.get()
        .uri("/accounts/{accountId}/transactions/{transactionsId}", data.getAccount().getId(), -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
}
