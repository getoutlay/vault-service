package io.gitlab.getoutlay;

import org.springframework.stereotype.Component;

@Component
public class TestData {

  private Account account;
  private Category category;
  private Transaction transaction;
  private TransactionCategory transactionCategory;

  public Account getAccount() {
    return account;
  }

  public Category getCategory() {
    return category;
  }

  public Transaction getTransaction() {
    return transaction;
  }

  public TransactionCategory getTransactionCategory() {
    return transactionCategory;
  }


  public void setAccount(Account account) {
    this.account = account;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public void setTransaction(Transaction transaction) {
    this.transaction = transaction;
  }

  public void setTransactionCategory(TransactionCategory transactionCategory) {
    this.transactionCategory = transactionCategory;
  }


  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("TestData {\n");
    sb.append(" account=").append(account).append("\n");
    sb.append(" category=").append(category).append("\n");
    sb.append(" transaction=").append(transaction).append("\n");
    sb.append(" transactionCategory=").append(transactionCategory).append("\n");
    sb.append('}');
    return sb.toString();
  }

}
