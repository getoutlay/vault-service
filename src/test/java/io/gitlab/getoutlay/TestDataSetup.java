package io.gitlab.getoutlay;

import java.time.Instant;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

public class TestDataSetup implements BeforeAllCallback {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private final Object lock = new Object();
  private static boolean initialized = false;

  @Override
  public void beforeAll(ExtensionContext context) {
    synchronized (lock) {
      if (!initialized) {
        initialized = true;
        logger.info("initializing test data...");

        final ApplicationContext ctx = SpringExtension.getApplicationContext(context);
        final TestData data = ctx.getBean(TestData.class);
        logger.debug("initial test data: {}", data);

        final AccountRepository accountRepository = ctx.getBean(AccountRepository.class);
        logger.debug("creating Account...");
        final Account account = new Account();
        account.setName("Malayan Tapir");
        account.setInstitution("Malayan Tapir");
        account.setAccountNumber("Malayan Tapir");
        account.setExpenseAccount(false);
        account.setCurrency(Currency.values()[0]);
        account.setReaderName("Malayan Tapir");
        data.setAccount(accountRepository.deleteAll()
            .then(accountRepository.save(account))
            .block());
        logger.info("test Account: {}", data.getAccount());

        final CategoryRepository categoryRepository = ctx.getBean(CategoryRepository.class);
        logger.debug("creating Category...");
        final Category category = new Category();
        category.setName("Malayan Tapir");
        category.setColorCode("Malayan Tapir");
        data.setCategory(categoryRepository.deleteAll()
            .then(categoryRepository.save(category))
            .block());
        logger.info("test Category: {}", data.getCategory());

        final TransactionRepository transactionRepository = ctx.getBean(TransactionRepository.class);
        logger.debug("creating Transaction...");
        final Transaction transaction = new Transaction();
        transaction.setAccountId(data.getAccount().getId());
        transaction.setDesription("Malayan Tapir");
        transaction.setAmount(87934588315.2384);
        transaction.setCurrency(Currency.values()[0]);
        transaction.setBookedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
        transaction.setExecutedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
        transaction.setExplanation("Malayan Tapir");
        transaction.setReference("Malayan Tapir");
        transaction.setCorresponderName("Malayan Tapir");
        transaction.setCorresponderBank("Malayan Tapir");
        transaction.setCorresponderAccount("Malayan Tapir");
        data.setTransaction(transactionRepository.deleteAll()
            .then(transactionRepository.save(transaction))
            .block());
        logger.info("test Transaction: {}", data.getTransaction());

        final TransactionCategoryRepository transactionCategoryRepository = ctx.getBean(TransactionCategoryRepository.class);
        logger.debug("creating TransactionCategory...");
        final TransactionCategory transactionCategory = new TransactionCategory();
        transactionCategory.setTransactionId(data.getTransaction().getId());
        transactionCategory.setCategoryId(data.getCategory().getId());
        transactionCategory.setAssignedOn(Instant.parse("2020-02-01T10:00:00.00Z"));
        transactionCategory.setAssignedBy("Malayan Tapir");
        data.setTransactionCategory(transactionCategoryRepository.deleteAll()
            .then(transactionCategoryRepository.save(transactionCategory))
            .block());
        logger.info("test TransactionCategory: {}", data.getTransactionCategory());

        logger.info("test data initialized: {}", data);
      }
    }
  }

}
