package io.gitlab.getoutlay;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;


@Table("T_CATEGORY")
public class Category {

  @Id
  private Long id;
  
  @NotNull
  @Column("NAME")
  @JsonProperty
  private String name;

  @NotNull
  @Column("COLOR_CODE")
  @JsonProperty
  private String colorCode;

  public Long getId() {
    return id;
  }

  
  public String getName() { return name; }

  public String getColorCode() { return colorCode; }

  public void setId(Long id) {
    this.id = id;
  }

  
  public void setName(String name) { this.name = name; }

  public void setColorCode(String colorCode) { this.colorCode = colorCode; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Category category = (Category) o;
    return Objects.equals(id, category.id)
        && Objects.equals(name, category.name)
        && Objects.equals(colorCode, category.colorCode);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Category{");
    sb.append("id=").append(id);
    sb.append(", name=").append(name);
    sb.append(", colorCode=").append(colorCode);
    sb.append('}');
    return sb.toString();
  }

}
