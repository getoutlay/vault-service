package io.gitlab.getoutlay;

import static io.gitlab.getoutlay.util.FluxUtils.wrapNotFoundIfEmpty;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/accounts")
public class AccountController {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final AccountRepository repository;
  private final TransactionRepository transactionsRepository;

  public AccountController(AccountRepository repository, TransactionRepository transactionsRepository) {
    this.repository = repository;
    this.transactionsRepository = transactionsRepository;
  }

  @GetMapping()
  public Flux<Account> listAll() {
    logger.debug("listAll()");
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public Mono<Account> getOne(@PathVariable Long id) {
    logger.debug("getOne({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "account", id);
  }

  @DeleteMapping("/{id}")
  public Mono<Account> delete(@PathVariable Long id) {
    logger.debug("delete({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id)
        .flatMap(account -> repository.delete(account).thenReturn(account)), "account", id);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Account> create(@RequestBody @Valid Account account) {
    logger.debug("create({})", account);
    return repository.save(account);
  }

  @PutMapping("/{id}")
  public Mono<Account> update(@PathVariable Long id, @RequestBody @Valid Account entity) {
    logger.debug("create({} : {})", id, entity);
    entity.setId(id);
    return wrapNotFoundIfEmpty(repository.save(entity), "account", id);
  }
  
  
  @GetMapping("/{id}/transactions")
  public Flux<Transaction> listAllTransactions(@PathVariable Long id) {
    logger.debug("listAllTransactions({})", id);
    return wrapNotFoundIfEmpty(getOne(id), "account", id)
        .flatMapMany(account -> transactionsRepository.findByAccountId(account.getId()));
  }

  @GetMapping("/{id}/transactions/{transactionsId}")
  public Mono<Transaction> getOneTransactions(@PathVariable Long id, @PathVariable Long transactionsId) {
    logger.debug("getOneTransactions({}, {})", id, transactionsId);
    return wrapNotFoundIfEmpty(getOne(id), "account", id)
        .flatMap(account -> wrapNotFoundIfEmpty(transactionsRepository.findById(transactionsId), "transactions", transactionsId));
  }

  @DeleteMapping("/{id}/transactions/{transactionsId}")
  public Mono<Transaction> deleteTransactions(@PathVariable Long id, @PathVariable Long transactionsId) {
    logger.debug("deleteTransactions({}, {})", id, transactionsId);
    return wrapNotFoundIfEmpty(getOne(id), "account", id)
        .flatMap(account -> wrapNotFoundIfEmpty(transactionsRepository.findById(transactionsId), "transactions", transactionsId))
        .flatMap(transactions -> transactionsRepository.delete(transactions).thenReturn(transactions));
  }

  @PostMapping("/{id}/transactions")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Transaction> createTransactions(@PathVariable Long id, @RequestBody @Valid Transaction transactions) {
    logger.debug("createTransactions({}, {})", id, transactions);
    return wrapNotFoundIfEmpty(getOne(id), "account", id)
        .flatMap(account -> {
          transactions.setAccountId(account.getId());
          return transactionsRepository.save(transactions);
        });
  }

  @PutMapping("/{id}/transactions/{transactionsId}")
  public Mono<Transaction> updateTransactions(@PathVariable Long id, @PathVariable Long transactionsId, @RequestBody @Valid Transaction transactions) {
    logger.debug("updateTransactions({}, {}, {})", id, transactionsId, transactions);
    return wrapNotFoundIfEmpty(getOne(id), "account", id)
        .flatMap(account -> {
          transactions.setId(transactionsId);
          return wrapNotFoundIfEmpty(transactionsRepository.save(transactions), "transactions", id);
        });
  }
  
}
