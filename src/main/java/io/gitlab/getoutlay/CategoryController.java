package io.gitlab.getoutlay;

import static io.gitlab.getoutlay.util.FluxUtils.wrapNotFoundIfEmpty;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/categories")
public class CategoryController {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final CategoryRepository repository;
  

  public CategoryController(CategoryRepository repository) {
    this.repository = repository;
    
  }

  @GetMapping()
  public Flux<Category> listAll() {
    logger.debug("listAll()");
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public Mono<Category> getOne(@PathVariable Long id) {
    logger.debug("getOne({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "category", id);
  }

  @DeleteMapping("/{id}")
  public Mono<Category> delete(@PathVariable Long id) {
    logger.debug("delete({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id)
        .flatMap(category -> repository.delete(category).thenReturn(category)), "category", id);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Category> create(@RequestBody @Valid Category category) {
    logger.debug("create({})", category);
    return repository.save(category);
  }

  @PutMapping("/{id}")
  public Mono<Category> update(@PathVariable Long id, @RequestBody @Valid Category entity) {
    logger.debug("create({} : {})", id, entity);
    entity.setId(id);
    return wrapNotFoundIfEmpty(repository.save(entity), "category", id);
  }
  
  
}
