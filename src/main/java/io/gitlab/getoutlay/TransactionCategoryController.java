package io.gitlab.getoutlay;

import static io.gitlab.getoutlay.util.FluxUtils.wrapNotFoundIfEmpty;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/transaction-categories")
public class TransactionCategoryController {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final TransactionCategoryRepository repository;
  private final TransactionRepository transactionRepository;
  private final CategoryRepository categoryRepository;

  public TransactionCategoryController(TransactionCategoryRepository repository, TransactionRepository transactionRepository, CategoryRepository categoryRepository) {
    this.repository = repository;
    this.transactionRepository = transactionRepository;
    this.categoryRepository = categoryRepository;
  }

  @GetMapping()
  public Flux<TransactionCategory> listAll() {
    logger.debug("listAll()");
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public Mono<TransactionCategory> getOne(@PathVariable Long id) {
    logger.debug("getOne({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "transactionCategory", id);
  }

  @DeleteMapping("/{id}")
  public Mono<TransactionCategory> delete(@PathVariable Long id) {
    logger.debug("delete({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id)
        .flatMap(transactionCategory -> repository.delete(transactionCategory).thenReturn(transactionCategory)), "transactionCategory", id);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<TransactionCategory> create(@RequestBody @Valid TransactionCategory transactionCategory) {
    logger.debug("create({})", transactionCategory);
    return repository.save(transactionCategory);
  }

  @PutMapping("/{id}")
  public Mono<TransactionCategory> update(@PathVariable Long id, @RequestBody @Valid TransactionCategory entity) {
    logger.debug("create({} : {})", id, entity);
    entity.setId(id);
    return wrapNotFoundIfEmpty(repository.save(entity), "transactionCategory", id);
  }
  
  @GetMapping("/{id}/transaction")
  public Mono<Transaction> getTransaction(@PathVariable Long id) {
    logger.debug("getTransaction({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "order", id)
        .flatMap(order -> {
          Long transactionId = order.getTransactionId();
          return wrapNotFoundIfEmpty(transactionRepository.findById(transactionId), "transaction", transactionId);
        });
  }
  
  @GetMapping("/{id}/category")
  public Mono<Category> getCategory(@PathVariable Long id) {
    logger.debug("getCategory({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "order", id)
        .flatMap(order -> {
          Long categoryId = order.getCategoryId();
          return wrapNotFoundIfEmpty(categoryRepository.findById(categoryId), "category", categoryId);
        });
  }
  
  
}
