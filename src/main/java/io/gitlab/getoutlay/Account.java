package io.gitlab.getoutlay;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;


@Table("T_ACCOUNT")
public class Account {

  @Id
  private Long id;
  
  @NotNull
  @Column("NAME")
  @JsonProperty
  private String name;

  @NotNull
  @Column("INSTITUTION")
  @JsonProperty
  private String institution;

  @NotNull
  @Column("ACCOUNT_NUMBER")
  @JsonProperty
  private String accountNumber;

  @NotNull
  @Column("EXPENSE_ACCOUNT")
  @JsonProperty
  private Boolean expenseAccount;

  @NotNull
  @Column("CURRENCY")
  @JsonProperty
  private Currency currency;

  @NotNull
  @Column("READER_NAME")
  @JsonProperty
  private String readerName;

  public Long getId() {
    return id;
  }

  
  public String getName() { return name; }

  public String getInstitution() { return institution; }

  public String getAccountNumber() { return accountNumber; }

  public Boolean getExpenseAccount() { return expenseAccount; }

  public Currency getCurrency() { return currency; }

  public String getReaderName() { return readerName; }

  public void setId(Long id) {
    this.id = id;
  }

  
  public void setName(String name) { this.name = name; }

  public void setInstitution(String institution) { this.institution = institution; }

  public void setAccountNumber(String accountNumber) { this.accountNumber = accountNumber; }

  public void setExpenseAccount(Boolean expenseAccount) { this.expenseAccount = expenseAccount; }

  public void setCurrency(Currency currency) { this.currency = currency; }

  public void setReaderName(String readerName) { this.readerName = readerName; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Account account = (Account) o;
    return Objects.equals(id, account.id)
        && Objects.equals(name, account.name)
        && Objects.equals(institution, account.institution)
        && Objects.equals(accountNumber, account.accountNumber)
        && Objects.equals(expenseAccount, account.expenseAccount)
        && Objects.equals(currency, account.currency)
        && Objects.equals(readerName, account.readerName);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Account{");
    sb.append("id=").append(id);
    sb.append(", name=").append(name);
    sb.append(", institution=").append(institution);
    sb.append(", accountNumber=").append(accountNumber);
    sb.append(", expenseAccount=").append(expenseAccount);
    sb.append(", currency=").append(currency);
    sb.append(", readerName=").append(readerName);
    sb.append('}');
    return sb.toString();
  }

}
