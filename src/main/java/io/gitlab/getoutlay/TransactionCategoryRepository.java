package io.gitlab.getoutlay;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface TransactionCategoryRepository extends ReactiveCrudRepository<TransactionCategory, Long> {
  Flux<TransactionCategory> findByTransactionId(Long transactionId);
  Flux<TransactionCategory> findByCategoryId(Long categoryId);
}
