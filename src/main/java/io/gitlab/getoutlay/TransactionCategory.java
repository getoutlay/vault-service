package io.gitlab.getoutlay;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import java.time.Instant;

@Table("T_TRANSACTION_CATEGORY")
public class TransactionCategory {

  @Id
  private Long id;
  
  @NotNull
  @Column("TRANSACTION_ID")
  @JsonProperty
  private Long transactionId;

  @NotNull
  @Column("CATEGORY_ID")
  @JsonProperty
  private Long categoryId;

  @NotNull
  @Column("ASSIGNED_ON")
  @JsonProperty
  private Instant assignedOn;

  @NotNull
  @Column("ASSIGNED_BY")
  @JsonProperty
  private String assignedBy;

  public Long getId() {
    return id;
  }

  
  public Long getTransactionId() { return transactionId; }

  public Long getCategoryId() { return categoryId; }

  public Instant getAssignedOn() { return assignedOn; }

  public String getAssignedBy() { return assignedBy; }

  public void setId(Long id) {
    this.id = id;
  }

  
  public void setTransactionId(Long transactionId) { this.transactionId = transactionId; }

  public void setCategoryId(Long categoryId) { this.categoryId = categoryId; }

  public void setAssignedOn(Instant assignedOn) { this.assignedOn = assignedOn; }

  public void setAssignedBy(String assignedBy) { this.assignedBy = assignedBy; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransactionCategory transactionCategory = (TransactionCategory) o;
    return Objects.equals(id, transactionCategory.id)
        && Objects.equals(transactionId, transactionCategory.transactionId)
        && Objects.equals(categoryId, transactionCategory.categoryId)
        && Objects.equals(assignedOn, transactionCategory.assignedOn)
        && Objects.equals(assignedBy, transactionCategory.assignedBy);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("TransactionCategory{");
    sb.append("id=").append(id);
    sb.append(", transactionId=").append(transactionId);
    sb.append(", categoryId=").append(categoryId);
    sb.append(", assignedOn=").append(assignedOn);
    sb.append(", assignedBy=").append(assignedBy);
    sb.append('}');
    return sb.toString();
  }

}
