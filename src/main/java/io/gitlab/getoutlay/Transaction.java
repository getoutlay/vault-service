package io.gitlab.getoutlay;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import java.time.Instant;

@Table("T_TRANSACTION")
public class Transaction {

  @Id
  private Long id;
  
  @NotNull
  @Column("ACCOUNT_ID")
  @JsonProperty
  private Long accountId;

  @NotNull
  @Column("DESRIPTION")
  @JsonProperty
  private String desription;

  @NotNull
  @Column("AMOUNT")
  @JsonProperty
  private Double amount;

  @NotNull
  @Column("CURRENCY")
  @JsonProperty
  private Currency currency;

  @NotNull
  @Column("BOOKED_ON")
  @JsonProperty
  private Instant bookedOn;

  @NotNull
  @Column("EXECUTED_ON")
  @JsonProperty
  private Instant executedOn;

  @NotNull
  @Column("EXPLANATION")
  @JsonProperty
  private String explanation;

  @NotNull
  @Column("REFERENCE")
  @JsonProperty
  private String reference;

  @NotNull
  @Column("CORRESPONDER_NAME")
  @JsonProperty
  private String corresponderName;

  @NotNull
  @Column("CORRESPONDER_BANK")
  @JsonProperty
  private String corresponderBank;

  @NotNull
  @Column("CORRESPONDER_ACCOUNT")
  @JsonProperty
  private String corresponderAccount;

  public Long getId() {
    return id;
  }

  
  public Long getAccountId() { return accountId; }

  public String getDesription() { return desription; }

  public Double getAmount() { return amount; }

  public Currency getCurrency() { return currency; }

  public Instant getBookedOn() { return bookedOn; }

  public Instant getExecutedOn() { return executedOn; }

  public String getExplanation() { return explanation; }

  public String getReference() { return reference; }

  public String getCorresponderName() { return corresponderName; }

  public String getCorresponderBank() { return corresponderBank; }

  public String getCorresponderAccount() { return corresponderAccount; }

  public void setId(Long id) {
    this.id = id;
  }

  
  public void setAccountId(Long accountId) { this.accountId = accountId; }

  public void setDesription(String desription) { this.desription = desription; }

  public void setAmount(Double amount) { this.amount = amount; }

  public void setCurrency(Currency currency) { this.currency = currency; }

  public void setBookedOn(Instant bookedOn) { this.bookedOn = bookedOn; }

  public void setExecutedOn(Instant executedOn) { this.executedOn = executedOn; }

  public void setExplanation(String explanation) { this.explanation = explanation; }

  public void setReference(String reference) { this.reference = reference; }

  public void setCorresponderName(String corresponderName) { this.corresponderName = corresponderName; }

  public void setCorresponderBank(String corresponderBank) { this.corresponderBank = corresponderBank; }

  public void setCorresponderAccount(String corresponderAccount) { this.corresponderAccount = corresponderAccount; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transaction transaction = (Transaction) o;
    return Objects.equals(id, transaction.id)
        && Objects.equals(accountId, transaction.accountId)
        && Objects.equals(desription, transaction.desription)
        && Objects.equals(amount, transaction.amount)
        && Objects.equals(currency, transaction.currency)
        && Objects.equals(bookedOn, transaction.bookedOn)
        && Objects.equals(executedOn, transaction.executedOn)
        && Objects.equals(explanation, transaction.explanation)
        && Objects.equals(reference, transaction.reference)
        && Objects.equals(corresponderName, transaction.corresponderName)
        && Objects.equals(corresponderBank, transaction.corresponderBank)
        && Objects.equals(corresponderAccount, transaction.corresponderAccount);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Transaction{");
    sb.append("id=").append(id);
    sb.append(", accountId=").append(accountId);
    sb.append(", desription=").append(desription);
    sb.append(", amount=").append(amount);
    sb.append(", currency=").append(currency);
    sb.append(", bookedOn=").append(bookedOn);
    sb.append(", executedOn=").append(executedOn);
    sb.append(", explanation=").append(explanation);
    sb.append(", reference=").append(reference);
    sb.append(", corresponderName=").append(corresponderName);
    sb.append(", corresponderBank=").append(corresponderBank);
    sb.append(", corresponderAccount=").append(corresponderAccount);
    sb.append('}');
    return sb.toString();
  }

}
