package io.gitlab.getoutlay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication(proxyBeanMethods = false)
@Configuration
@EnableConfigurationProperties(io.gitlab.getoutlay.db.DatabaseMigrationProperties.class)
//mastic-pin-annotation
public class VaultServiceApplication {

  public VaultServiceApplication() {
  }

  public static void main(String[] args) {
    SpringApplication.run(VaultServiceApplication.class, args);
  }

}
