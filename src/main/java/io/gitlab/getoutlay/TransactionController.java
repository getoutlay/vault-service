package io.gitlab.getoutlay;

import static io.gitlab.getoutlay.util.FluxUtils.wrapNotFoundIfEmpty;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final TransactionRepository repository;
  private final AccountRepository accountRepository;

  public TransactionController(TransactionRepository repository, AccountRepository accountRepository) {
    this.repository = repository;
    this.accountRepository = accountRepository;
  }

  @GetMapping()
  public Flux<Transaction> listAll() {
    logger.debug("listAll()");
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public Mono<Transaction> getOne(@PathVariable Long id) {
    logger.debug("getOne({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "transaction", id);
  }

  @DeleteMapping("/{id}")
  public Mono<Transaction> delete(@PathVariable Long id) {
    logger.debug("delete({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id)
        .flatMap(transaction -> repository.delete(transaction).thenReturn(transaction)), "transaction", id);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Transaction> create(@RequestBody @Valid Transaction transaction) {
    logger.debug("create({})", transaction);
    return repository.save(transaction);
  }

  @PutMapping("/{id}")
  public Mono<Transaction> update(@PathVariable Long id, @RequestBody @Valid Transaction entity) {
    logger.debug("create({} : {})", id, entity);
    entity.setId(id);
    return wrapNotFoundIfEmpty(repository.save(entity), "transaction", id);
  }
  
  @GetMapping("/{id}/account")
  public Mono<Account> getAccount(@PathVariable Long id) {
    logger.debug("getAccount({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "order", id)
        .flatMap(order -> {
          Long accountId = order.getAccountId();
          return wrapNotFoundIfEmpty(accountRepository.findById(accountId), "account", accountId);
        });
  }
  
  
}
